# Word Paths

## Problem definition
This program finds "word paths."  What this means is that you find a path from one word to another word, changing one letter each step, and each intermediate word must be in the dictionary.
 
Some example solutions:
 
    flux -> flex -> flem -> alem
    rial -> real -> feal -> foal -> foul -> foud
    dung -> dunt -> dent -> gent -> geet -> geez
    doeg -> dong -> song -> sing -> sink -> sick
    jehu -> jesu -> jest -> gest -> gent -> gena -> guna -> guha
    broo -> brod -> brad -> arad -> adad -> adai -> admi -> ammi -> immi
    yagi -> yali -> pali -> palp -> paup -> plup -> blup
    bitt -> butt -> burt -> bert -> berm -> germ -> geum -> meum
    jina -> pina -> pint -> pent -> peat -> prat -> pray
    fike -> fire -> fare -> care -> carp -> camp
 
The program takes as input the path to the word file, a start word, and an end word, and prints out one or more paths from start to end, or indicates that there is no possible path.

## Usage

    usage: ./jwordpath <startWord> <endWord> [-1] [-h] [-q]
        Find a word path from startWord to endWord.
             -1,--first   do not display progress while running
             -q,--quiet   return only the first path found (default to finding
                          shortest path)
             -h,--help    print this message
    
    usage: ./jwordpath <word>
        Display all the words in the dictionary with the
        same length as the given word, sorted by simularity
        to given word.

## Development

This project is configured to use junit-4.11, with hamcrest-core-1.3. 





