package main.java;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang3.StringUtils;

class WordPathCLI {
	public static void main(String[] args) {
	    CommandLineParser parser = new PosixParser();
	    Options options = new Options();
	    options.addOption("h", "help", false, "print this message");
	    options.addOption("q", "quiet", false, "return only the first path found (default to finding shortest path)");
	    options.addOption("1", "first", false, "do not display progress while running");
        
		try {
			CommandLine line = parser.parse(options, args);
			String[] cliArgs = line.getArgs();
			if (cliArgs.length < 1 || cliArgs.length > 2 || line.hasOption("help")) {
				System.out.println("usage: jwordpath <startWord> <endWord> [-1] [-h] [-q]");
				System.out.println("    Find a word path from startWord to endWord.");
				System.out.println("		 -1,--first   do not display progress while running");
				System.out.println("		 -q,--quiet   return only the first path found (default to finding");
				System.out.println("		              shortest path)");
				System.out.println("		 -h,--help    print this message");
				System.out.println("");
				System.out.println("usage: jwordpath <word>");
				System.out.println("    Display all the words in the dictionary with the");
				System.out.println("    same length as the given word, sorted by simularity");
				System.out.println("    to given word.");
		    } else {
				WordPathFinder wpf = new WordPathFinder(!line.hasOption("quiet"), line.hasOption("first"));
				if (cliArgs.length == 1) {
					List<String> candidateWords = wpf.getCandidateWords(cliArgs[0]);
					System.out.println("wordpath " + StringUtils.join(candidateWords," "));
				} else if (cliArgs.length == 2) {
					List<String> wordPath = wpf.find(cliArgs[0], cliArgs[1]);
					System.out.println("Found:\033[K\n  " + StringUtils.join(wordPath," -> ") + " (" + wordPath.size() + ")");
				}
		    }
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
