package main.java;

import java.util.ArrayList;
import java.util.HashSet;

public class WordPath {
	
	private ArrayList<String> words = new ArrayList<>();
	private HashSet<String> wordsSet = new HashSet<>();
	
	public ArrayList<String> getWords() {
		return words;
	}

	public static int countDifferences(String word1, String word2) {
		int diffCount = 0;
	    for (int i=0, len=word1.length(); i<len; i++) {
	        if (word1.charAt(i) != word2.charAt(i)) {
	        	diffCount += 1;
	        }
	    }        
	    return diffCount;
	}

	public boolean appendIfAble(String word) {
		if (!words.isEmpty()) {
			String lastWord = words.get(words.size()-1);
			if (word.length() != lastWord.length()) {
				return false;
			}
			if (wordsSet.contains(word)) {
				return false;
			}
			if (countDifferences(word, lastWord) != 1) {
				return false;
			}
		}
		words.add(word);
		wordsSet.add(word);
		return true;
	}

	public void removeLast() {
		int lastIndex = words.size() - 1;
		if (lastIndex >= 0) {
			wordsSet.remove(words.get(lastIndex));
			words.remove(lastIndex);
		}
	}

}
