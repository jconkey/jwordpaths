package main.java;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Dictionary {
	public static String DICTIONARY_FILE = "/usr/share/dict/words";
	private HashMap<Integer, Set<String>> wordsByLength = new HashMap<>();
	
	public Dictionary() {
	}
	
	public void loadDictionary() {
		FileReader file;
		try {
			file = new FileReader(DICTIONARY_FILE);
			loadDictionaryFile(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void loadDictionaryFile(Readable file) {
		wordsByLength = new HashMap<>();
		for(Scanner sc = new Scanner(file); sc.hasNext();) {
			String word = sc.nextLine().trim().toLowerCase();
			int len = word.length();
			if (!wordsByLength.containsKey(len) ) {
				wordsByLength.put(len, new HashSet<String>());
			}
			wordsByLength.get(len).add(word);
		}
	}

	public Set<String> getWordsOfLength(int len) {
		return wordsByLength.get(len);
	}
	
	public boolean isDefined(String word) {
		int len = word.length();
		return wordsByLength.containsKey(len) && wordsByLength.get(len).contains(word);
	}
}
