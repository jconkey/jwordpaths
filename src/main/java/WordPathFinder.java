package main.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class WordPathFinder {

	protected Dictionary dictionary;
	protected ArrayList<String> shortestWordPath;
	protected String endWord;
	protected List<String> candidateWordsSortedList;
	protected boolean verbose;
	protected boolean onlyFirstResult;
	
	public WordPathFinder(boolean verbose, boolean onlyFirstResult) {
		this.verbose = verbose;
		this.onlyFirstResult = onlyFirstResult;
		SetupDictionary();
	}
	
	public void SetupDictionary() {
		dictionary = new Dictionary();
		dictionary.loadDictionary();
	}

	public List<String> getCandidateWords(final String targetWord) {
		return sortWordsByLetterDifferences(
				dictionary.getWordsOfLength(targetWord.length()), targetWord);
	}

	private List<String> sortWordsByLetterDifferences(Set<String> words, final String targetWord) {
		List<String> candidateWordsSorted = new ArrayList<String>(words);
		
		Collections.sort(candidateWordsSorted, new Comparator<String>() {
		    @Override
		    public int compare(String word1, String word2) {
		    	int diff1 = WordPath.countDifferences(word1, targetWord);
		    	int diff2 = WordPath.countDifferences(word2, targetWord);
	            if (diff1 < diff2) {
	                return -1;
	            } else if (diff1 > diff2) {
	                return 1;
	            }
	            return 0;
            }
		});
		
		return candidateWordsSorted;
	}

	public List<String> find(String startWord, String endWord) {
		WordPath initialWordPath;
		shortestWordPath = new ArrayList<>();
		
        startWord = startWord.toLowerCase();
        endWord = endWord.toLowerCase();

        if (dictionary.isDefined(startWord) && dictionary.isDefined(endWord)) {
            this.endWord = endWord;
            candidateWordsSortedList = getCandidateWords(endWord);
            initialWordPath = new WordPath();
            initialWordPath.appendIfAble(startWord);
            this.findHelper(initialWordPath);
        }

		return shortestWordPath;
	}

	public boolean findHelper(WordPath wordPath) {
		if (verbose) {
            String s = String.join(" -> ", wordPath.getWords());
            int len = s.length();
            if (len > 100) {
                s = s.substring(0, 25) + " ... " + s.substring(len-70);
            }
            System.out.print("\033[K  " + s + "\r");
        }
		
		for (String candidateWord: candidateWordsSortedList) {
			if (wordPath.appendIfAble(candidateWord)) {
				int swpl = shortestWordPath.size();
				int nwpl = wordPath.getWords().size();
				if (swpl == 0 || nwpl < swpl) {
					// wordPath is shorter than current shortest word path
					if (candidateWord.equals(endWord)) {
						// end case
						shortestWordPath = new ArrayList<>(wordPath.getWords());
						if (verbose) {
							System.out.println("  " + String.join(" -> ", shortestWordPath)
                                + " (" + shortestWordPath.size() + ")");
                        }
						if (onlyFirstResult) {
	                        return true;
	                    }
					} else {
						// recursive case
						if (findHelper(wordPath)) {
                            return true;
                        }
					}
				}
            	wordPath.removeLast();
			}
		}
		return false;
	}
}
