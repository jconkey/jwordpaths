package main.test;

import static org.junit.Assert.*;

import java.io.StringReader;

import main.java.Dictionary;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class DictionaryTest {

	public static String[] testWords = {
		"A", "flux", "flex", "flem", "alem", "rial",
		"real", "feal", "foal", "foul", "foud",
		"to", "by", "me", "we", "my"
	};
	
	@Test
	public void loadsDictionaryFile() {
		Dictionary d = new Dictionary();
//		System.out.println("test words: " + StringUtils.join(testWords,"\n"));
		d.loadDictionaryFile(new StringReader(StringUtils.join(testWords,"\n")));
		assertTrue("did not load words", 0 < d.getWordsOfLength(4).size());
		assertTrue("did not lowercase words", d.getWordsOfLength(1).contains("a"));
	}
	
	@Test
	public void groupsWordsByLength() {
		Dictionary d = new Dictionary();
		d.loadDictionaryFile(new StringReader(StringUtils.join(testWords,"\n")));
        assertEquals("did not load exactly one 1-char word", 1, d.getWordsOfLength(1).size());
        assertEquals("did not load exactly ten 4-char words", 10, d.getWordsOfLength(4).size());
        assertEquals("did not load exactly five 2-char words", 5, d.getWordsOfLength(2).size());
	}

}
