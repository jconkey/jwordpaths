package main.test;

import static org.junit.Assert.*;

import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

import main.java.Dictionary;
import main.java.WordPathFinder;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

class MockedWordPathFinder extends WordPathFinder {
	public MockedWordPathFinder(boolean verbose, boolean onlyFirstResult) {
		super(verbose, onlyFirstResult);
	}

	public static String[] testWords = {
		"A", "flux", "flex", "flem", "alem", "rial",
		"real", "feal", "foal", "foul", "foud",
		"to", "by", "me", "we", "my"
	};
	
	public void SetupDictionary() {
		dictionary = new Dictionary();
		dictionary.loadDictionaryFile(new StringReader(StringUtils.join(testWords,"\n")));
	}
}

public class WordPathFinderTest {

	@Test
	public void shouldGetCandidateWords() {
		WordPathFinder wpf = new MockedWordPathFinder(true, false);
		List<String> candidateWords = wpf.getCandidateWords("we");
		assertEquals(Arrays.asList("we", "me", "by", "to", "my"), candidateWords);
	}
	
	@Test
	public void shouldReturnEmptyListIfNoPathIsPossible() {
		WordPathFinder wpf = new MockedWordPathFinder(true, false);
		assertEquals("returned a path with invalid words", 0, wpf.find("flux", "dinosaur").size());
		assertEquals("returned a path with invalid words", 0, wpf.find("alem", "my").size());
	}
	
	@Test
	public void shouldReturnValidPath() {
		WordPathFinder wpf = new MockedWordPathFinder(true, false);
		assertEquals("did not return a path with valid words", 
			Arrays.asList("flux", "flex"), wpf.find("flux", "flex"));
		assertEquals("did not return a path with valid words", 
			Arrays.asList("real", "feal", "foal", "foul", "foud"), wpf.find("real", "foud"));
		assertEquals("did not return a path with valid words", 
			Arrays.asList("by", "my", "me", "we"), wpf.find("by", "we"));
	}
}
