package main.test;

import static org.junit.Assert.*;
import main.java.WordPath;

import org.junit.Test;

public class WordPathTest {

	@Test
	public void makesInstance() {
		WordPath wp = new WordPath();	
		assertEquals("didn't create instance with empty word list", 0, wp.getWords().size());
	}
	
	@Test
	public void countsDifferences() {
        assertEquals("got wrong difference count", 0, WordPath.countDifferences("flux", "flux"));
        assertEquals("got wrong difference count", 1, WordPath.countDifferences("flux", "flex"));
        assertEquals("got wrong difference count", 3, WordPath.countDifferences("abc", "xyz"));
	}
	
	@Test
	public void canAppend() {
		WordPath wp = new WordPath();	
        
		assertEquals("didn't return true to indicate word was added", true, wp.appendIfAble("flux"));
        assertEquals("didn't add valid word", 1, wp.getWords().size());
        assertEquals("didn't add given word", "flux", wp.getWords().get(0));

		assertEquals("didn't return true to indicate word was added", true, wp.appendIfAble("flex"));
        assertEquals("didn't add valid word", 2, wp.getWords().size());
        assertEquals("didn't add given word", "flex", wp.getWords().get(1));

        // can we add duplicate word?
        assertEquals("returned true to indicate word was added", false, wp.appendIfAble("flux"));
        assertEquals("added invalid word", 2, wp.getWords().size());

        assertEquals("returned true to indicate word was added", false, wp.appendIfAble("flex"));
        assertEquals("added invalid word", 2, wp.getWords().size());

        assertEquals("returned true to indicate word was added", false, wp.appendIfAble("x"));
        assertEquals("added invalid word", 2, wp.getWords().size());
	}
	
	@Test
	public void canRemoveLast() {
		WordPath wp = new WordPath();	
		assertEquals("did not add word", true, wp.appendIfAble("hack"));
		wp.removeLast();
		assertEquals("did not remove word", 0, wp.getWords().size());
	}
}
